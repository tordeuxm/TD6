/**
 * Affiche la carte d'un pokémon passé en paramètre
 * 
 * @param nameOrIndex nom ou index du pokémon à afficher
 */
async function addPokemon(nameOrIndex) {
    try {
        let req = await fetch(`${ROOT_URL}/pokemon/${nameOrIndex}/`);
        let data = await req.json();
        addPokemonCard(data);
    } catch (error) {
        console.log(error);
    }
}


/**
 * Affiche les cartes correspondant à chacune des espèces apparaissant dans la chaîne d'évolution de l'espèce passée en paramètre
 * 
 * @param nameOrIndex espèce de départ de la chaîne d'évolution
 */
async function addEvolutionChain(nameOrIndex) {
    const response = await fetch(`${ROOT_URL}/pokemon-species/${nameOrIndex}/`);
    const data = await response.json();
    const response2 = await fetch(data.evolution_chain.url);
    const data2 = await response2.json();
    let urls = getSpeciesUrls(data2.chain);
    for(let i = 0 ; i < urls.length ; i++){
        const response3 = await fetch(urls[i]);
        const data3 = await response3.json();
        addPokemon(data3.name);

    }
}
