/**
 * @param url adresse à laquelle envoyer la requête GET
 * @returns une promesse représentant let données JSON récupérées depuis l'URL passée en paramètre
 */
function getData(url) {
    return new Promise(function (resolve, reject) {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.onload = function () {
            if (xhr.status === 200) {
                const data = JSON.parse(xhr.responseText);
                resolve(data);
            } else {
                reject(new Error(xhr.responseText));
            }
        }
        xhr.send();
    });
}


/**
 * Affiche la carte d'un pokémon passé en paramètre
 *
 * @param nameOrIndex nom ou index du pokémon à afficher
 */
function addPokemon(nameOrIndex) {
    getData(`${ROOT_URL}/pokemon/${nameOrIndex}/`)
        .then(data => addPokemonCard(data))
        .catch(error => console.log(error));
}


/**
 * Affiche les cartes correspondant à chacune des espèces apparaissant dans la chaîne d'évolution de l'espèce passée en paramètre
 *
 * @param nameOrIndex espèce de départ de la chaîne d'évolution
 */
function addEvolutionChain(nameOrIndex) {
    getData(`${ROOT_URL}/pokemon-species/${nameOrIndex}/`).
    then(data=> getData(data.evolution_chain.url).
        then(data2 => {
            let urls = getSpeciesUrls(data2.chain);
            for(let i = 0 ; i < urls.length ; i++){
                getData(urls[i]).then(data3 => addPokemon(data3.name)).catch((error) => {
        console.log(error.message)})
        }}).

        catch((error) => {
        console.log(error.message)})).
    catch((error) => {
        console.log(error.message)});
}
