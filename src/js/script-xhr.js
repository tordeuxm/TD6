/**
 * Affiche la carte d'un pokémon passé en paramètre
 *
 * @param nameOrIndex nom ou index du pokémon à afficher
 */
function addPokemon(nameOrIndex) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `${ROOT_URL}/pokemon/${nameOrIndex}/`, true);
    xhr.onload = function () {
        if (xhr.status === 200) {
            const data = JSON.parse(xhr.responseText);
            addPokemonCard(data);
        } else {
            console.log(Error(xhr.statusText));
        }
    }
    xhr.send();
}


/**
 * Affiche les cartes correspondant à chacune des espèces apparaissant dans la chaîne d'évolution de l'espèce passée en paramètre
 *
 * @param nameOrIndex espèce de départ de la chaîne d'évolution
 */
function addEvolutionChain(nameOrIndex) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `${ROOT_URL}/pokemon-species/${nameOrIndex}/`, true);
    xhr.onload = function () {
        callback2(xhr);
    }
    xhr.send();
}


function callback2(req) {
    if (req.status === 200) {
        const data = JSON.parse(req.responseText);
        const xhr2 = new XMLHttpRequest();
        xhr2.open('GET', data.evolution_chain.url, true);
        xhr2.onload = function () {
            callback3(xhr2);
        }
        xhr2.send();

    } else {
        console.log(Error(req.statusText));
    }
}

function callback3(req) {
    if (req.status === 200) {
        const data2 = JSON.parse(req.responseText);
        $urls = getSpeciesUrls(data2.chain);
        for (let i = 0; i < $urls.length; i++) {
            const xhr3 = new XMLHttpRequest();
            xhr3.open('GET', $urls[i], true);
            xhr3.onload = function () {
                if (xhr3.status === 200) {
                    const data3 = JSON.parse(xhr3.responseText);
                    addPokemon(data3.name);
                } else {
                    console.log(Error(xhr3.statusText));
                }
            }
            xhr3.send();
        }
    } else {
        console.log(Error(req.statusText));
    }
}