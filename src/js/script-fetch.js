/**
 * Affiche la carte d'un pokémon passé en paramètre
 *
 * @param nameOrIndex nom ou index du pokémon à afficher
 */
function addPokemon(nameOrIndex) {
    fetch(`${ROOT_URL}/pokemon/${nameOrIndex}/`)
        .then(response => response.json())
        .then(data => addPokemonCard(data))
        .catch(error => console.log(error));
}


/**
 * Affiche les cartes correspondant à chacune des espèces apparaissant dans la chaîne d'évolution de l'espèce passée en paramètre
 *
 * @param nameOrIndex espèce de départ de la chaîne d'évolution
 */
function addEvolutionChain(nameOrIndex) {
    fetch(`${ROOT_URL}/pokemon-species/${nameOrIndex}/`).
    then(response => response.json()).
    then(data => fetch(data.evolution_chain.url).
        then(response2 => response2.json()).
        then(data2 => {
            let urls = getSpeciesUrls(data2.chain);
            for (let i = 0 ; i < urls.length ; i++){
               fetch(urls[i]).
               then(response3 => response3.json()).
               then(data3 => addPokemon(data3.name)).catch(error => console.log(error))
            }
        }).catch(error => console.log(error))
    ).catch(error => console.log(error));

}
